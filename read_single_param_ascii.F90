!itb_param...subroutine reads single-point ASCII parameter file

subroutine read_single_param_ascii(sib,time,filename)

use kinds
use sibtype
use timetype

use sib_const_module

implicit none

character(len=100),intent(in) :: filename

type(sib_t), dimension(subcount), intent(inout) :: sib
type(time_struct), intent(inout) :: time

!itb-----------------------------------

!itb_param...local variables

real(kind=dbl_kind) :: dummy

real(kind=dbl_kind) :: t_start,t_end,t_date,t_fpar,t_zlt,t_green,t_z0,   &
                       t_zpd,t_rbc,t_rdc,t_gmudmu,t_d13cr,t_pf1,t_pf2,   &
                       t_pf3,t_pf4,t_pf5

integer(kind=int_kind) :: ntest1,i0,indx,temp_loop

character(len=4) :: cyear



!itb----------------------------------

print*,'ASCII:',trim(filename)
 
       open(unit=32, file=trim(filename), form='formatted')
        read(32,*)ntest1
        if(ntest1.ne.nsib)  stop &
            ' file sib_bc no match with model for nsib'

        ! read in time-invariant boundary condition variables
        read(32,*)
        read(32,*)
        read(32,*) sib%param%biome
        read(32,*) dummy
                sib%param%phystype(1) = dummy
        read(32,*) dummy
                sib%param%phystype(2) = dummy
        read(32,*) dummy
                sib%param%phystype(3) = dummy
        read(32,*) dummy
                sib%param%phystype(4) = dummy
        read(32,*) dummy
                sib%param%phystype(5) = dummy

        read(32,*) sib%param%z2
        read(32,*) sib%param%z1
        read(32,*) sib%param%vcover
        read(32,*) sib%param%chil
        read(32,*) sib%param%phc
        read(32,*) sib%param%tran(1,1)
        read(32,*) sib%param%tran(1,2)
        read(32,*) sib%param%tran(2,1)
        read(32,*) sib%param%tran(2,2)
        read(32,*) sib%param%ref(1,1)
        read(32,*) sib%param%ref(1,2)
        read(32,*) sib%param%ref(2,1)
        read(32,*) sib%param%ref(2,2)
        read(32,*) sib%param%vmax0(1)
        read(32,*) sib%param%vmax0(2)
        read(32,*) sib%param%vmax0(3)
        read(32,*) sib%param%vmax0(4)
        read(32,*) sib%param%vmax0(5)
        read(32,*) sib%param%effcon(1)
        read(32,*) sib%param%effcon(2)
        read(32,*) sib%param%effcon(3)
        read(32,*) sib%param%effcon(4)
        read(32,*) sib%param%effcon(5)
        read(32,*) sib%param%gradm(1)
        read(32,*) sib%param%gradm(2)
        read(32,*) sib%param%gradm(3)
        read(32,*) sib%param%gradm(4)
        read(32,*) sib%param%gradm(5)
        read(32,*) sib%param%binter(1)
        read(32,*) sib%param%binter(2)
        read(32,*) sib%param%binter(3)
        read(32,*) sib%param%binter(4)
        read(32,*) sib%param%binter(5)
        read(32,*) sib%param%atheta(1)
        read(32,*) sib%param%atheta(2)
        read(32,*) sib%param%atheta(3)
        read(32,*) sib%param%atheta(4)
        read(32,*) sib%param%atheta(5)
        read(32,*) sib%param%btheta(1)
        read(32,*) sib%param%btheta(2)
        read(32,*) sib%param%btheta(3)
        read(32,*) sib%param%btheta(4)
        read(32,*) sib%param%btheta(5)
        read(32,*) sib%param%trda(1)
        read(32,*) sib%param%trda(2)
        read(32,*) sib%param%trda(3)
        read(32,*) sib%param%trda(4)
        read(32,*) sib%param%trda(5)
        read(32,*) sib%param%trdm(1)
        read(32,*) sib%param%trdm(2)
        read(32,*) sib%param%trdm(3)
        read(32,*) sib%param%trdm(4)
        read(32,*) sib%param%trdm(5)
        read(32,*) sib%param%trop(1)
        read(32,*) sib%param%trop(2)
        read(32,*) sib%param%trop(3)
        read(32,*) sib%param%trop(4)
        read(32,*) sib%param%trop(5)
        read(32,*) sib%param%respcp(1)
        read(32,*) sib%param%respcp(2)
        read(32,*) sib%param%respcp(3)
        read(32,*) sib%param%respcp(4)
        read(32,*) sib%param%respcp(5)
        read(32,*) sib%param%slti(1)
        read(32,*) sib%param%slti(2)
        read(32,*) sib%param%slti(3)
        read(32,*) sib%param%slti(4)
        read(32,*) sib%param%slti(5)
        read(32,*) sib%param%shti(1)
        read(32,*) sib%param%shti(2)
        read(32,*) sib%param%shti(3)
        read(32,*) sib%param%shti(4)
        read(32,*) sib%param%shti(5)
        read(32,*) sib%param%hltii(1)
        read(32,*) sib%param%hltii(2)
        read(32,*) sib%param%hltii(3)
        read(32,*) sib%param%hltii(4)
        read(32,*) sib%param%hltii(5)
        read(32,*) sib%param%hhti(1)
        read(32,*) sib%param%hhti(2)
        read(32,*) sib%param%hhti(3)
        read(32,*) sib%param%hhti(4)
        read(32,*) sib%param%hhti(5)
        read(32,*) sib%param%soref(1)
        read(32,*) sib%param%soref(2)
        read(32,*) sib%param%bee
        read(32,*) sib%param%phsat
        read(32,*) sib%param%satco
        read(32,*) sib%param%poros
        read(32,*) sib%param%slope
        read(32,*) sib%param%wopt
        read(32,*) sib%param%wsat
        read(32,*) sib%param%zm
        read(32,*) sib%param%sandfrac
        read(32,*) sib%param%clayfrac
        read(32,*) nper
        if(nper>npermax)  stop ' too many composite periods in param file'


!itb_param...read time-varying information
        
!itb_param...read header

!itb...may need to alter loop index
print*,'bc_recnum=',time%bc_recnum
        if(time%bc_recnum > 0 ) then
          temp_loop = time%bc_recnum
        else
          temp_loop = nper + time%bc_recnum
        endif

print*,'TEMP_LOOP=',temp_loop

        read(32,*)
        do i0 = 1,temp_loop

           read(32,*)indx,t_start,t_end,t_date,t_fpar,t_zlt,t_green,t_z0,   &
                     t_zpd,t_rbc,t_rdc,t_gmudmu,t_d13cr,t_pf1,t_pf2,   &
                     t_pf3,t_pf4,t_pf5


        enddo

        close(32)

!itb...check to make sure that we are reading d13c of het resp in...
        if(t_pf1 < 0.0) stop 'BAD VALUE FOR PHYSFRAC 1'

!itb_param...assign parameters
!        time%modis_start(time%bc_recnum) = t_start
!        time%modis_stop(time%bc_recnum)  = t_end

         sib%param%modis_time2 = t_date

!print*,'read_single_param: start=',sib%param%modis_time2

!        sib%param%modis_time3 = t_end

!itb...a little patch to fool the time manager, which is accustomed
!itb...to Andrew Philpott's complex NDVI interpolation scheme, and
!itb...uses 3 ndvi values. When we're reading from the ASCII file,
!itb...this has already been done in the mapper phase...
       if(time%bc_recnum > 0) then
         time%modis_start(time%bc_recnum) = t_date
       endif

        sib%param%aparc2       = t_fpar
        sib%param%mfpar2       = t_fpar

        sib%param%zlt2         = t_zlt
        sib%param%mlai2        = t_zlt

        sib%param%green2       = t_green
        sib%param%z0d2          = t_z0
        sib%param%zp_disp2     = t_zpd
        sib%param%rbc2         = t_rbc
        sib%param%rdc2         = t_rdc
        sib%param%gmudmu2      = t_gmudmu
        sib%param%d13c_het     = t_d13cr
        sib%param%physfrac2(1) = t_pf1
        sib%param%physfrac2(2) = t_pf2
        sib%param%physfrac2(3) = t_pf3
        sib%param%physfrac2(4) = t_pf4
        sib%param%physfrac2(5) = t_pf5

!print*,'physfrac:',t_pf1,t_pf2,t_pf3,t_pf4,t_pf5
!print*,'read_single_param_ascii:',t_fpar,t_zlt,t_green,t_z0,t_zpd,t_rdc,t_rbc,t_gmudmu,t_d13cr,t_pf1

!itb...need to calculate woptzm

sib(1)%param%woptzm = (sib(1)%param%wopt/100.0) ** sib(1)%param%zm



end subroutine read_single_param_ascii




subroutine single_ascii_composite(sib,time,filename)

use kinds
use sibtype
use timetype

use sib_const_module

implicit none

character(len=100),intent(in) :: filename

type(sib_t), dimension(subcount), intent(inout) :: sib
type(time_struct), intent(inout) :: time

!itb---------------------------------

!itb...local vars

   integer(kind=int_kind) :: i0,i1
   integer(kind=int_kind) :: ntest1

!itb---------------------------------

   open(unit=32, file=trim(filename), form='formatted')
   read(32,*)ntest1
   if(ntest1.ne.nsib)  stop &
            ' file sib_bc no match with model for nsib'


!itb...bypass time-invariant information
    do i0 = 1, 103
      read(32,*)
    enddo

!itb...read number of periods
    read(32,*)nper
    if(nper>npermax)  stop ' too many composite periods in param file'

!itb...skip header
    read(32,*)

!itb...read start/stop dates for compositing periods

    do i0 = 1, nper

      read(32,*) i1,time%modis_start(i0), time%modis_stop(i0)
!print*,'single_ascii_comp:',i0,time%modis_start(i0),time%modis_stop(i0)

    enddo

    close(32)

end subroutine single_ascii_composite
