!
!===============================================================================
subroutine respfactor_control( sib, time, rank )
!===============================================================================
! Controls all calculations associated with the respfactors
!
! Modifications:
!  Kevin Schaefer added variables for autotrophic respfactor (5/6/05)
!
use kinds
use timetype
use sib_const_module
use sib_io_module
use sibtype
!
implicit none
!
! parameters
type(sib_t), dimension(subcount), intent(inout) :: sib
type(time_struct), intent(in) :: time
integer(kind=int_kind), intent(in) :: rank

! local variables
integer(kind=int_kind) :: i, s, m
real(kind=dbl_kind) test
!
! add new values to sums of annual totals
   do i = 1, subcount
      sib(i)%param%tot_an(time%month) = sib(i)%param%tot_an(time%month) +  &
            sib(i)%diag%assimn(6) * time%dtsib
      sib(i)%param%tot_gpp(time%month) = sib(i)%param%tot_gpp(time%month) +  &
            sib(i)%diag%assim(6) * time%dtsib
      sib(i)%param%tot_rc(time%month) = sib(i)%param%tot_rc(time%month) +  &
            sib(i)%diag%resp_can(6) * time%dtsib
      sib(i)%param%tot_fpar(time%month) = sib(i)%param%tot_fpar(time%month) +  &
            sib(i)%param%aparc * time%dtsib
      sib(i)%param%tot_nee(time%month) = sib(i)%param%tot_nee(time%month) +  &
            (sib(i)%diag%resp_tot-sib(i)%diag%assim(6))* time%dtsib
      sib(i)%param%tot_het(time%month) = sib(i)%param%tot_het(time%month) +  &
            sib(i)%diag%resp_het * time%dtsib
      sib(i)%param%tot_auto(time%month) = sib(i)%param%tot_auto(time%month) +  &
            sib(i)%diag%resp_auto * time%dtsib

!itb_iso 
      sib(i)%param%tot_d13c(time%month) =  sib(i)%param%tot_d13c(time%month) +   &
            sib(i)%diag%assimn(6) * time%dtsib * sib(i)%diag%d13cassimn(6)
!itb_iso 

      Do s = 1, nsoil
         sib(i)%param%tot_ss(time%month,s) =  &
            sib(i)%param%tot_ss(time%month,s) +   &
            sib(i)%diag%soilscale(s) * time%dtsib
      enddo
   enddo
!
! if new year, calculate new respfactors
   if ( time%calc_respf ) then
!
! add up 12 monthly totals to get annual total
      do i = 1, subcount
!
! clear out annual totals
         sib(i)%param%tot_an(13) = 0.0_dbl_kind
         sib(i)%param%tot_gpp(13) = 0.0_dbl_kind
         sib(i)%param%tot_rc(13) = 0.0_dbl_kind
         sib(i)%param%tot_fpar(13) = 0.0_dbl_kind
         sib(i)%param%tot_ss(13,:) = 0.0_dbl_kind
         sib(i)%param%tot_nee(13) = 0.0_dbl_kind
         sib(i)%param%tot_het(13) = 0.0_dbl_kind
         sib(i)%param%tot_auto(13) = 0.0_dbl_kind

!itb_iso
         sib(i)%param%tot_d13c(13) = 0.0_dbl_kind
!itb_iso 
!
! add up monthly totals
         do m = 1, 12
            sib(i)%param%tot_an(13) = sib(i)%param%tot_an(13) +  &
               sib(i)%param%tot_an(m)
            sib(i)%param%tot_gpp(13) = sib(i)%param%tot_gpp(13) +  &
               sib(i)%param%tot_gpp(m)
            sib(i)%param%tot_rc(13) = sib(i)%param%tot_rc(13) +  &
                sib(i)%param%tot_rc(m)
            sib(i)%param%tot_fpar(13) = sib(i)%param%tot_fpar(13) +  &
                sib(i)%param%tot_fpar(m)
            sib(i)%param%tot_nee(13) = sib(i)%param%tot_nee(13) +  &
                sib(i)%param%tot_nee(m)
            sib(i)%param%tot_het(13) = sib(i)%param%tot_het(13) +  &
                sib(i)%param%tot_het(m)
            sib(i)%param%tot_auto(13) = sib(i)%param%tot_auto(13) +  &
                sib(i)%param%tot_auto(m)

!itb_iso
            sib(i)%param%tot_d13c(13) = sib(i)%param%tot_d13c(13) +   &
                sib(i)%param%tot_d13c(m)
!itb_iso 


            do s = 1, nsoil
               sib(i)%param%tot_ss(13,s) = sib(i)%param%tot_ss(13,s) +  &
                  sib(i)%param%tot_ss(m,s)
            enddo
         enddo

      enddo
!
! test print messages
      i=1
      do m = 1, 12
         print*, m, sib(i)%param%tot_rc(m),sib(i)%param%tot_gpp(m)
      enddo
      do m = 1, 12
         print*, m, sib(i)%param%tot_gpp(m)-sib(i)%param%tot_rc(m)-sib(i)%param%tot_an(m)
      enddo
      print*, 'ann nee=',sib(i)%param%tot_nee(13)
      print*, 'test nee=',sib(i)%param%tot_het(13) + sib(i)%param%tot_auto(13) + sib(i)%param%tot_rc(13)- sib(i)%param%tot_gpp(13)
      print*, 'ann an=',sib(i)%param%tot_an(13)
      print*, 'ann gpp=',sib(i)%param%tot_gpp(13)
      print*, 'ann het=',sib(i)%param%tot_het(13)
      print*, 'ann aut=',sib(i)%param%tot_auto(13)
      print*, 'ann can=',sib(i)%param%tot_rc(13),sib(i)%param%tot_gpp(13)-sib(i)%param%tot_an(13)
      print*, 'ann fpar=',sib(i)%param%tot_fpar(13)
      print*, 'het ratio=',sib(i)%param%tot_het(13)/sib(i)%param%tot_gpp(13)
      print*, 'aut ratio=',(sib(i)%param%tot_auto(13) + sib(i)%param%tot_rc(13))/sib(i)%param%tot_gpp(13)
!
! calculate new respfactors
      call calc_respfactor( sib )
!
! write respfactors to file
      if ( time%write_respf ) then
!itb_merra
!        if(drvr_type=='single' .OR. drvr_type == 'ncp_sngl') then
        if(drvr_type=='single' .OR. drvr_type == 'ncp_sngl' .OR. drvr_type == 'mer_sngl') then

          call write_single_respfactor(sib,time)
        else
          call write_global_respfactor ( sib, time, rank )
        endif
    endif
!
! clear out totals
      if(roll_respf) then ! clear out next month's totals only
         m = mod( time%month, 12 ) + 1
         do i = 1, subcount
            sib(i)%param%tot_an(m) = 0.0_dbl_kind
            sib(i)%param%tot_gpp(m) = 0.0_dbl_kind
            sib(i)%param%tot_rc(m) = 0.0_dbl_kind
            sib(i)%param%tot_fpar(m) = 0.0_dbl_kind
            sib(i)%param%tot_nee(m) = 0.0_dbl_kind
            sib(i)%param%tot_het(m) = 0.0_dbl_kind
            sib(i)%param%tot_auto(m) = 0.0_dbl_kind
            sib(i)%param%tot_ss(m,:) = 0.0_dbl_kind
!itb_iso 
            sib(i)%param%tot_d13c(m) = 0.0_dbl_kind
!itb_iso 
         enddo
      else ! clear out all totals
         do i = 1, subcount
            sib(i)%param%tot_an(:) = 0.0_dbl_kind
            sib(i)%param%tot_gpp(:) = 0.0_dbl_kind
            sib(i)%param%tot_rc(:) = 0.0_dbl_kind
            sib(i)%param%tot_fpar(:) = 0.0_dbl_kind
            sib(i)%param%tot_nee(:) = 0.0_dbl_kind
            sib(i)%param%tot_het(:) = 0.0_dbl_kind
            sib(i)%param%tot_auto(:) = 0.0_dbl_kind
            sib(i)%param%tot_ss(:,:) = 0.0_dbl_kind
!itb_iso 
            sib(i)%param%tot_d13c(:) = 0.0_dbl_kind
!itb_iso 
         enddo
      endif
    endif
!
end subroutine respfactor_control
!
!===============================================================================
subroutine calc_respfactor( sib )
!===============================================================================
!  calculate the annual respiration rate "respfactor" for each of 7
!   soil layer at each grid cell in the model, given monthly mean
!   maps of net carbon assimilation and "soilscale" at each level.

!  references:
!  denning et al., 1996a, tellus 48b, 521-542
!  denning at al., 1996b, tellus 48b, 543-567 

!  soilscale is the product of a temperature response and a moisture
!   response function, evaluated in each soil layer. it is also called
!   r* in denning et al (1996a), equations 6 through 9, and in
!   denning et al (1996b), equations 8 and 9

!  note: the soilscale qp3 from bugs has only 6 layers, so 
!  *** before calling respire, you must "fill in" the bottom (first)
!      layer of soilscale with zeros ****
!
! Modifications:
!  Kevin Schaefer added autotrophic respfactor (5/6/05)
!------------------------------------------------------------------------------
!
use kinds
use sibtype
use sib_const_module
!
implicit none
!
! parameters
type(sib_t), dimension(subcount), intent(inout) :: sib
!                      
! local variables
integer :: n,l               ! looping indices
real(kind=dbl_kind) xag       ! above ground biomass fraction (litter)
real(kind=dbl_kind) xbg       ! below ground biomass fraction
real(kind=dbl_kind) xagmin    ! min above-grnd biomass fraction (low GPP ecosystems)
real(kind=dbl_kind) xagmax    ! max above-grnd biomass fraction (high GPP ecosystems)  
real(kind=dbl_kind) anainflec ! tot_an at inflection point for xag function
real(kind=dbl_kind) kxag      ! exponential constant for xag function    
real(kind=dbl_kind) tot_het   ! total annual heterotrophic respiration
real(kind=dbl_kind) tot_auto  ! total annual autotrophic respiration
real(kind=dbl_kind) rcfrac    ! fraction of GPP to canopy autotrophic respiration
!
parameter(xagmin = 0.10, xagmax = 0.75, anainflec = 1000.,  &
    kxag=5.e-3)
!
! loop through applicable grid points
    do n = 1, subcount
!
! divide total annual GPP into heterotrophic and autotrophic respiration
        rcfrac=sib(n)%param%tot_rc(13)/sib(n)%param%tot_gpp(13)
        if(rcfrac<=autofrac) then
          tot_het=(1.-autofrac)*sib(n)%param%tot_gpp(13)
          tot_auto=autofrac*sib(n)%param%tot_gpp(13)-sib(n)%param%tot_rc(13)
        else
          tot_het=sib(n)%param%tot_gpp(13)-sib(n)%param%tot_rc(13)
          if(tot_het<0.) print*, n, sib(n)%param%tot_gpp(13)-sib(n)%param%tot_rc(13), sib(n)%param%tot_an(13)
          tot_auto=0.
        endif
!
! split biomass into above ground (litter) and below ground
        ! above-ground biomass fraction
        xag = xagmin + (xagmax - xagmin) /  &
          ( 1.0 + exp( -kxag * ( tot_het * 12. - anainflec ) ) )

        ! below-ground biomass fraction
        xbg = 1.0 - xag
!
! vertical distribution of biomass proportional to root distribution
        ! top two soil layers include litter
        sib(n)%param%het_respfac(1) = tot_het * ( 0.5 * xag  +  &
            xbg * sib(n)%param%rootf(1))

        sib(n)%param%het_respfac(2) = tot_het * ( 0.5 * xag  +  &
            xbg * sib(n)%param%rootf(2))

        ! rooting layers (3..10)
        do l = 3, nsoil
            sib(n)%param%het_respfac(l) = tot_het * xbg  &
                * sib(n)%param%rootf(l)
        end do
!
! heterotrophic respiration factor: divide by annual soilscale
        do l = 1, nsoil
            sib(n)%param%het_respfac(l) = sib(n)%param%het_respfac(l) /  &
                sib(n)%param%tot_ss(13,l)
        end do ! (next layer)
!
! autotrophic respiration factor
        sib(n)%param%auto_respfac=tot_auto/sib(n)%param%tot_fpar(13)

!
!itb_iso 
! del13C of heterotrophic respiration
        if(sib(n)%param%tot_an(13) > 0.0) then
          sib(n)%param%d13c_het = sib(n)%param%tot_d13c(13) /      &
                                        sib(n)%param%tot_an(13)
        else
          sib(n)%param%d13c_het = sib(n)%prog%d13cm   ! del 13C of mixed layer 
                                                   ! for non-productive gridcells
        endif
!itb_iso 
 
    End do ! (next grid cell)

end subroutine calc_respfactor
!
!==================================================================
subroutine write_global_respfactor ( sib, time, rank )
!==================================================================
! This subroutine creates a netcdf respfactor file.
!
! Modifications:
!  Kevin Schaefer created routine (5/16/05)
!------------------------------------------------------------------
!
use kinds
#ifdef PGF
use netcdf
use typeSizes
#endif
use sibtype
use timetype
use sib_io_module
use sib_const_module
!
Implicit none
!
! inputs
type(sib_t), dimension(subcount), intent(in) :: sib  ! SiB variable tree
type(time_struct), intent(in) :: time       ! time data
integer(kind=int_kind), intent(in) :: rank  ! processor number
!
! netcdf id variables
integer(kind=int_kind) :: status      ! netcdf error number
integer(kind=int_kind) :: ncid        ! netcdf file id number
integer(kind=int_kind) :: did_time    ! dimension id - time
integer(kind=int_kind) :: did_nsib    ! dimension id - nsib
integer(kind=int_kind) :: did_nsoil   ! dimension id - nsoil
integer(kind=int_kind) :: did_lat     ! dimension id - latitude
integer(kind=int_kind) :: did_lon     ! dimension id - longitude
integer(kind=int_kind) :: did_subcount ! dimension id - subcount
integer(kind=int_kind) :: did_char    ! dimension id - character
integer(kind=int_kind) :: vid_time    ! variable id - time
integer(kind=int_kind) :: vid_start   ! variable id - start time
integer(kind=int_kind) :: vid_end     ! variable id - end time
integer(kind=int_kind) :: vid_period  ! variable id - period of time
integer(kind=int_kind) :: vid_lon     ! variable id - longitude
integer(kind=int_kind) :: vid_lat     ! variable id - latitude
integer(kind=int_kind) :: vid_lonindx ! variable id - longitude index
integer(kind=int_kind) :: vid_latindx ! variable id - latitude index
integer(kind=int_kind) :: vid_sibindx ! variable id - sib point index
integer(kind=int_kind) :: vid_subc    ! variable id - subcount
integer(kind=int_kind) :: vid_het     ! variable id - heterotrophic respfac
integer(kind=int_kind) :: vid_auto    ! variable id - autotrophic respfac
integer(kind=int_kind) :: vid_nsec    ! variable ID - time
!itb_iso 
integer(kind=int_kind) :: vid_d13cr   ! variable ID - del13C of het respiration
!itb_iso 
!
!
! local variables
integer(kind=int_kind) :: i,j,k,l,m,n       ! indeces
character*8 txt                         ! year and processor in character form
character*256 filename                      ! filename
integer(kind=int_kind) :: nsectemp          ! local time
real(kind=dbl_kind) het_respfac(subcount,nsoil) ! local heterotrophic respfactor
real(kind=dbl_kind) auto_respfac(subcount)      ! local autotrophic respfactor
!

!itb_iso
real(kind=dbl_kind) d13c_het_resp(subcount)     ! local del13C or het resp
!itb_iso 



! transfer data to local arrays 
    do i = 1, subcount
       auto_respfac(i) = sib(i)%param%auto_respfac

!itb_iso 
       d13c_het_resp(i) = sib(i)%param%d13c_het
!itb_iso 

       do j = 1, nsoil
          het_respfac(i,j) = sib(i)%param%het_respfac(j)
       enddo
    enddo
!
! local time variable
    nsectemp = time%year
!
! open respfactor file
    write(txt, '(i4.4,a,i3.3)') time%year, 'p', rank
    filename = trim(out_path)//"CO2_respf_"//trim(txt)//".nc" !jk
    print*, 'write global respfactor ', trim(filename)
    status = nf90_create( filename, nf90_clobber, ncid )
!
! define global attributes
    call global_atts( ncid, 'sib3', 'lat/lon', '1.0', drvr_type,  &
        biome_source, soil_source, soref_source, ndvi_source, c4_source,  &
        d13cresp_source, rank )
!
! define dimensions
    status = nf90_def_dim( ncid, 'time', nf90_unlimited, did_time )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',1)
    status = nf90_def_dim( ncid, 'nsib', nsib, did_nsib )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',2)
    status = nf90_def_dim( ncid, 'level', nsoil, did_nsoil )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',3)
    status = nf90_def_dim( ncid, 'char_len', 10, did_char )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',4)
    status = nf90_def_dim( ncid, 'latitude', jhr, did_lat )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',5)
    status = nf90_def_dim( ncid, 'longitude', ihr, did_lon )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',6)
    status = nf90_def_dim( ncid, 'landpoints', subcount, did_subcount )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',7)
!
! define latitude/longitude index variables
    status = nf90_def_var( ncid, 'time', nf90_double, (/did_time/), vid_time )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',8)
    status = nf90_put_att( ncid, vid_time, 'quantity', 'time' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',9)
    status = nf90_put_att( ncid, vid_time, 'units', 'year' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',10)
    status = nf90_put_att( ncid, vid_time, 'calender', 'noleap' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',11)
    
    status = nf90_def_var( ncid, 'start_period', nf90_int, (/did_time/), vid_start )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',12)
    status = nf90_put_att( ncid, vid_start, 'long_name', 'start of respfactor period' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',13)
    status = nf90_put_att( ncid, vid_start, 'units', 'day of year' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',14)
    
    status = nf90_def_var( ncid, 'end_period', nf90_int, (/did_time/), vid_end )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',15)
    status = nf90_put_att( ncid, vid_end, 'long_name', 'end of respfactor period' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',16)
    status = nf90_put_att( ncid, vid_end, 'units', 'day of year' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',17)
    
    status = nf90_def_var( ncid, 'period_length', nf90_double, (/did_time/), vid_period )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',18)
    status = nf90_put_att( ncid, vid_period, 'long_name', 'length of respfactor period' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',19)
    status = nf90_put_att( ncid, vid_period, 'units', 'days' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',20)

    status = nf90_def_var( ncid, 'latitude', nf90_float, (/did_lat/), vid_lat )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',21)
    status = nf90_put_att( ncid, vid_lat, 'units', 'degrees_north' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',22)
    status = nf90_put_att( ncid, vid_lat, 'quantity', 'latitude' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',23)
    
    status = nf90_def_var( ncid, 'longitude', nf90_float, (/did_lon/), vid_lon )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',24)
    status = nf90_put_att( ncid, vid_lon, 'units', 'degrees_east' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',25)
    status = nf90_put_att( ncid, vid_lon, 'quantity', 'longitude' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',26)

    status = nf90_def_var( ncid, 'lonindex', nf90_int, (/did_subcount/), vid_lonindx )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',27)
    status = nf90_put_att( ncid, vid_lonindx, 'long_name', 'Longitude array index' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',28)
    status = nf90_put_att( ncid, vid_lonindx, 'units', 'index-integer' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',29)

    status = nf90_def_var( ncid, 'latindex', nf90_int, (/did_subcount/), vid_latindx )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',30)
    status = nf90_put_att( ncid, vid_latindx, 'long_name', 'Latitude array index' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',31)
    status = nf90_put_att( ncid, vid_latindx, 'units', 'index-integer' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',32)

    status = nf90_def_var( ncid, 'sibindex', nf90_int, (/did_subcount/), vid_sibindx )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',30)
    status = nf90_put_att( ncid, vid_sibindx, 'long_name', 'subset to nsib array index' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',31)
    status = nf90_put_att( ncid, vid_sibindx, 'units', 'index-integer' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',32)
!
! define heterotrophic respiration factor variable
    status = nf90_def_var( ncid, 'het_respfac', nf90_double, (/did_subcount,did_nsoil/), vid_het)
    if(status/=nf90_noerr) call handle_err(status,'rf_write',33)
    status = nf90_put_att( ncid, vid_het, 'long_name', 'Heterotrophic Respiration Factor' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',34)
    status = nf90_put_att( ncid, vid_het, 'title', 'Heterotrophic Respiration Factor' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',35)
    status = nf90_put_att( ncid, vid_het, 'units', 'moles/m2/s' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',36)
    status = nf90_put_att( ncid, vid_het, 'missing_value', 1.e36 )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',37)
!
! define autotrophic respiration factor variable
    status = nf90_def_var( ncid, 'auto_respfac', nf90_double, (/did_subcount/), vid_auto)
    if(status/=nf90_noerr) call handle_err(status,'rf_write',38)
    status = nf90_put_att( ncid, vid_auto, 'long_name', 'Autotrophic Respiration Factor' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',39)
    status = nf90_put_att( ncid, vid_auto, 'title', 'Autotrophic Respiration Factor' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',40)
    status = nf90_put_att( ncid, vid_auto, 'units', 'moles/m2/s' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',41)
    status = nf90_put_att( ncid, vid_auto, 'missing_value', 1.e36 )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',42)

!
!itb_iso 
! define del13C of autotrophic respiration factor variable
    status = nf90_def_var( ncid, 'del13c_resp_het', nf90_double,      &
                                (/did_subcount/), vid_d13cr)
    if(status/=nf90_noerr) call handle_err(status,'rf_write',421)
    status = nf90_put_att( ncid, vid_d13cr, 'long_name',      &
                                'del13C of Heterotrophic Respiration' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',422)
    status = nf90_put_att( ncid, vid_d13cr, 'title',      &
                                'del13C of Heterotrophic Respiration' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',423)
    status = nf90_put_att( ncid, vid_d13cr, 'units', 'none' )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',424)
    status = nf90_put_att( ncid, vid_d13cr, 'missing_value', 1.e36 )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',425)
!itb_iso 

!
! end variable definition
    status = nf90_enddef( ncid )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',43)
!
! assign values to grid and time related variables
    status = nf90_put_var( ncid, vid_time, nsectemp )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',44)
    status = nf90_put_var( ncid, vid_start, 1 )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',45)
    status = nf90_put_var( ncid, vid_end, 365 )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',46)
    status = nf90_put_var( ncid, vid_period, 365 )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',47)
    status = nf90_put_var( ncid, vid_lat, latitude )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',48)
    status = nf90_put_var( ncid, vid_lon, longitude )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',49)
    status = nf90_put_var( ncid, vid_lonindx, sublon )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',50)
    status = nf90_put_var( ncid, vid_latindx, sublat )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',51)
    status = nf90_put_var( ncid, vid_sibindx, subset )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',51)
!
! load respfactor variables
    status = nf90_put_var( ncid, vid_het, het_respfac )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',52)
    status = nf90_put_var( ncid, vid_auto, auto_respfac )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',53)

!itb_iso 
    status = nf90_put_var( ncid, vid_d13cr, d13c_het_resp )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',531)
!itb_iso 
!
! close respfactor file
    status = nf90_close( ncid )
    if(status/=nf90_noerr) call handle_err(status,'rf_write',54)
!
end subroutine write_global_respfactor
!
!===============================================================================
subroutine write_single_respfactor(sib,time)
!===============================================================================
! writes respfactor to ascii file for single point
!
! Modifications:
!  Kevin Schaefer created routine (7/22/05)
!-------------------------------------------------------------------------------
!
use kinds
use sibtype
use timetype
use sib_const_module
use sib_io_module
!
implicit none
!
! input/output variables
type(sib_t), dimension(subcount), intent(inout) :: sib
type(time_struct), intent(in) :: time   ! time data
!
! local variables
integer(kind=int_kind) :: i,j, k, l,n,m ! indeces
character*8 txt                         ! year and processor in character form
character*256 filename                  ! filename

logical good  ! flag indicating good data
real(kind=dbl_kind), allocatable :: loc_data(:,:) ! temp var to read in data
!
! Open SiB-CO2 respiration factor 
    write(txt, '(i4.4)') time%year
    filename = trim(out_path)//'CO2_respf_'//trim(txt) !jk
    open( unit=3, file=trim(filename), form='formatted')
!
! print message to screen
    print*, '\t write single respfactor ', trim(filename)
!
! write heterotrophic respfactor
    do i = 1,nsoil
      write(3,*) sib(1)%param%het_respfac(i),' het_respfac lev',i
    enddo
!
! write autotrophic respfactor
    write(3,*) sib(1)%param%auto_respfac,' auto_respfac'

!
!itb_iso 
! write del13C of heterotrophic respiration
    write(3,*) sib(1)%param%d13c_het,' d13C_het_resp'
!itb_iso 

!
! close file
    close (3)
!
end subroutine write_single_respfactor
