subroutine sibdrv_read_ncep2_single( sib, time )

!****--------------------------------------------------------------------
!    This subroutines reads the forcing surface meteorological data 
!    for the next time step.
!    If required, it closes the current month's data file and opens the 
!    next month's data file.

!    precip, snowfall and radiation data are aggregated data over 6 hours
!    They have been stored such that the data at time x represent the
!    aggregation over the preceeding 6 hours.
!    Thus data at 
!     0 hours are the sum of the values 18-24 hours of the previous day
!     6 hours are the sum of the values  0- 6 hours of the current day
!    12 hours are the sum of the values  6-12 hours of the current day
!    18 hours are the sum of the values 12-18 hours of the current day
!
!    Consequently new data are read for all variables at the same time.
!    For all point data variables, e.g. temp., the data are for
!    now + 6 hours. For precip and radiation data the new data
!    are the aggregation over the next six hours.
!
! Modifications:
!  Kevin Schaefer moved conversion pascals to millibars from sibdrv_interp to here (8/16/04)
!  Kevin Schaefer changed from tdew1/2 to sh1/2 because thats whats there (8/17/04)
!****--------------------------------------------------------------------


use sib_const_module, only:   &
    nsib,               &
    latsib,             &
    lonsib,             &
    subset,             &
    subcount

use sib_io_module, only:   &
    dr_format,           &
    driver_id

use physical_parameters, only:              &
    kapa => kappa,   &
    pi

use kinds
#ifdef PGF
use netcdf
use typeSizes
#endif
use sibtype
use timetype

implicit none


type(sib_t), dimension(subcount), intent(inout) :: sib
type(time_struct), intent(in) :: time




integer(kind=int_kind) :: i, iyear, imon, iday, idoy, ihour, imin

integer(kind=int_kind) ::  n

character*80 filename
character*7 gchar
integer(kind=int_kind) :: nctimeid,ncyid,ncmid,nctdid,ncdoyid,nchid
integer(kind=int_kind), dimension(2) :: mstart,mcount

integer(kind=int_kind) :: nct2mid ! Total Cloud Cover
integer(kind=int_kind) :: nctccid ! Total Cloud Cover
integer(kind=int_kind) :: ncswdid ! Surface solar radiation downwards           
integer(kind=int_kind) :: ncldwid ! Surface thermal radiation downwards
integer(kind=int_kind) :: ncuwdid ! U-wind at 10 m
integer(kind=int_kind) :: ncvwdid ! V-wind at 10 m
integer(kind=int_kind) :: ncshid  ! humidity at 2 m
integer(kind=int_kind) :: ncsfpid ! Log Surface Pressure
integer(kind=int_kind) :: nclspid ! Large Scale Precipitation
integer(kind=int_kind) :: nccvpid ! Convective Precipitation
integer(kind=int_kind) :: ncsflid ! Snow Fall

!itb_ncep_single
integer(kind=int_kind),parameter :: nsib_global=14637
integer(kind=int_kind) :: ipoint
!itb_ncep_single

real(kind=real_kind), dimension(nsib_global) :: t2m ! Total Cloud Cover
real(kind=real_kind), dimension(nsib_global) :: tcc ! Total Cloud Cover
real(kind=real_kind), dimension(nsib_global) :: swd ! Surface solar radiation downwards           
real(kind=real_kind), dimension(nsib_global) :: ldw ! Surface thermal radiation downwards
real(kind=real_kind), dimension(nsib_global) :: sh  ! humidity at 2 m
real(kind=real_kind), dimension(nsib_global) :: sfp ! Log Surface Pressure
real(kind=real_kind), dimension(nsib_global) :: lsp ! Large Scale Precipitation
real(kind=real_kind), dimension(nsib_global) :: cvp ! Convective Precipitation
real(kind=real_kind), dimension(nsib_global) :: sfl ! Snow Fall

integer(kind=int_kind) :: status
real(kind=real_kind) :: xtime,xyear,xmonth,xdoy,xday,xhour
real(kind=real_kind), dimension(nsib_global) :: xx,uwd,vwd



character(len=23) :: subname
data subname/'sibdrv_read_single_ncep '/









   !*** Storing previous time steps data
    do i=1,subcount
        sib(i)%prog%ps1       = sib(i)%prog%ps2
        sib(i)%prog%tm1       = sib(i)%prog%tm2
        sib(i)%prog%tcc1      = sib(i)%prog%tcc2
        sib(i)%prog%sh1       = sib(i)%prog%sh2
        sib(i)%prog%spdm1     = sib(i)%prog%spdm2
        sib(i)%prog%lspr1     = sib(i)%prog%lspr2
        sib(i)%prog%cupr1     = sib(i)%prog%cupr2
        sib(i)%prog%dlwbot1   = sib(i)%prog%dlwbot2
        sib(i)%prog%sw_dwn1 = sib(i)%prog%sw_dwn2

!itb_ncep_single...
        ipoint = sib(i)%param%pt_1x1
!        print*,'ipoint=',ipoint

    enddo

    ! switch files if needed
    if ( time%switch_driver ) then
        status = nf90_close( driver_id )

        write( filename, dr_format ) time%driver_year, time%driver_month


        status = nf90_open( trim(filename), nf90_nowrite, driver_id )
        if(status/=nf90_noerr) call handle_err(status,'read_ncep2',1)
    endif

    ! Read new driver data


    ! check time values in driver data file
!    status = nf90_inq_varid( driver_id,   'time', nctimeid )
!    if ( status /= nf90_noerr ) call handle_err( status )
    status = nf90_inq_varid( driver_id,   'year', ncyid )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',2)
    status = nf90_inq_varid( driver_id,   'month',ncmid )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',3)
    status = nf90_inq_varid( driver_id,   'doy',  ncdoyid )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',4)
    status = nf90_inq_varid( driver_id,   'day',  nctdid )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',5)
    status = nf90_inq_varid( driver_id,   'hour', nchid )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',6)

    ! read time
    mstart(1) = time%driver_recnum
!    status = nf90_get_var( driver_id, nctimeid, xtime, mstart(1:1) )
!    if ( status /= nf90_noerr ) call handle_err( status )
    status = nf90_get_var( driver_id, ncyid,    xyear, mstart(1:1) )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',7)
    status = nf90_get_var( driver_id, ncmid,   xmonth, mstart(1:1) )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',8)
    status = nf90_get_var( driver_id, ncdoyid,   xdoy, mstart(1:1) )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',9)
    status = nf90_get_var( driver_id, nctdid,    xday, mstart(1:1) )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',10)
    status = nf90_get_var( driver_id, nchid,    xhour, mstart(1:1) )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',11)

    ihour=xhour
    iday =xday
    idoy =xdoy
    imon =xmonth
    iyear=xyear
    imin=0

!    print*,subname,'Time level in file: ',ihour,iday,idoy,imon,iyear
!    print*,'driver_recnum=',time%driver_recnum
!    print*,'--------'

    !* Get variable id's
    status = nf90_inq_varid( driver_id, 't2m', nct2mid ) ! Temperature at 2 m
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',12)
    status = nf90_inq_varid( driver_id, 'tcc', nctccid ) ! Total Cloud Cover
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',13)
    status = nf90_inq_varid( driver_id, 'swd', ncswdid ) ! Surface solar rad downwards
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',14)
    status = nf90_inq_varid( driver_id, 'lwd', ncldwid ) ! Surface thermal rad down
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',15)
    status = nf90_inq_varid( driver_id, 'uwd', ncuwdid ) ! U-wind at 10 m
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',16)
    status = nf90_inq_varid( driver_id, 'vwd', ncvwdid ) ! V-wind at 10 m
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',17)
    status = nf90_inq_varid( driver_id, 'shum', ncshid ) ! humidity at 2 m
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',18)
    status = nf90_inq_varid( driver_id, 'sfp', ncsfpid ) ! Log Surface Pressure
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',19)
    status = nf90_inq_varid( driver_id, 'lsp', nclspid ) ! Large Scale Precipitation
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',20)
    status = nf90_inq_varid( driver_id, 'cvp', nccvpid ) ! Convective Precipitation
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',21)
    status = nf90_inq_varid( driver_id, 'sfl', ncsflid ) ! Snow Fall
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',22)


    !* get data
    mstart=(/1,time%driver_recnum/); mcount=(/nsib_global,1/)
    status = nf90_get_var( driver_id, nct2mid, t2m,    & !Temperature at 2 m
         mstart,  mcount )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',23)
    status = nf90_get_var( driver_id, nctccid, tcc,    & !Total Cloud Cover
         mstart,  mcount )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',24)
    status = nf90_get_var( driver_id, ncswdid, swd,    & !Surface solar rad downwards
         mstart,  mcount )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',25)
    status = nf90_get_var( driver_id, ncldwid, ldw,    & !Surface thermal rad downwards
         mstart,  mcount )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',26)
    status = nf90_get_var( driver_id, ncuwdid, uwd,    & ! U-wind at 10 m
         mstart,  mcount )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',27)
    status = nf90_get_var( driver_id, ncvwdid, vwd,    & ! V-wind at 10 m
         mstart,  mcount )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',28)
    status = nf90_get_var( driver_id, ncshid, sh,      & ! humidity at 2 m
         mstart,  mcount )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',29)
    status = nf90_get_var( driver_id, ncsfpid, sfp,    & ! Surface Pressure
         mstart,  mcount )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',30)
    status = nf90_get_var( driver_id, nclspid, lsp,    & ! Large Scale Precipitation
         mstart,  mcount )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',31)
    status = nf90_get_var( driver_id, nccvpid, cvp,    & ! Convective Precipitation
         mstart,  mcount )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',32)
    status = nf90_get_var(driver_id, ncsflid, xx,    & ! Snow Fall
         mstart,  mcount )
    if(status/=nf90_noerr) call handle_err(status,'read_ncep2',33)




!itb...I have this hardwired for now: user must figure out the point
!itb...he/she is seeking (not hard to do) and then directly assign it


        ! pull out landpoints in subdomain
        sib(1)%prog%tm2 = t2m(ipoint)
        sib(1)%prog%tcc2 = tcc(ipoint)
        sib(1)%prog%sw_dwn2 = swd(ipoint)
        sib(1)%prog%dlwbot2 = ldw(ipoint)
        sib(1)%prog%sh2 = sh(ipoint)
        sib(1)%prog%ps2 = sfp(ipoint)
        sib(1)%prog%lspr2 = lsp(ipoint)
        sib(1)%prog%cupr2 = cvp(ipoint)
    
        ! scale radiation to w/m2
        !sib(1)%prog%sw_dwn2 = sib(1)%prog%sw_dwn2/time%driver_step
        !sib(1)%prog%dlwbot2 = sib(1)%prog%dlwbot2/time%driver_step
        if ( sib(1)%prog%sw_dwn2 < 0 ) sib(1)%prog%sw_dwn2 = 0.0
        if ( sib(1)%prog%dlwbot2 < 0 ) sib(1)%prog%dlwbot2 = 0.0
        
        ! convert total cloud cover to fraction
        sib(1)%prog%tcc2 = sib(1)%prog%tcc2 * 0.01

        ! 10 m wind
        sib(1)%prog%spdm2=SQRT(uwd(ipoint)*uwd(ipoint)+vwd(ipoint)*vwd(ipoint))

        ! add snowfall to large scale precip and let SiB decide about snow.
        sib(1)%prog%lspr2 = sib(1)%prog%lspr2+xx(ipoint)
        ! convert to mm
        sib(1)%prog%lspr2 = (sib(1)%prog%lspr2-sib(1)%prog%cupr2)*time%driver_step
        sib(1)%prog%cupr2 = sib(1)%prog%cupr2*time%driver_step
        ! make sure precip > 0
        if ( sib(1)%prog%lspr2 < 0.0 ) sib(1)%prog%lspr2 = 0.0
        if ( sib(1)%prog%cupr2 < 0.0 ) sib(1)%prog%cupr2 = 0.0

        ! Conversion Pa -> hPa (pascals to millibars)
        sib(1)%prog%ps2 = sib(1)%prog%ps2 * 0.01

!    print*,subname,' New driver data read ',ihour,iday,imon,iyear
!    print*,'------------------------------------------------------'
!    print*,'Extrema of new input data'
!    print*, minval(sib%prog%tm2      ),maxval(sib%prog%tm2  ),' Temperature'
!    print*, minval(sib%prog%tcc2     ),maxval(sib%prog%tcc2),' Total cloudiness'
!    print*, minval(sib%prog%sh2    ),maxval(sib%prog%sh2 ),' dew point'
!    print*, minval(sib%prog%spdm2    ),maxval(sib%prog%spdm2),' Surface wind'
!    print*, minval(sib%prog%ps2      ),maxval(sib%prog%ps2 ),' Pressure'
!    print*, minval(sib%prog%dlwbot2  ),maxval(sib%prog%dlwbot2),  &
!        ' Long wave down'
!    print*, minval(sib%prog%lspr2    ),maxval(sib%prog%lspr2),' Large sc precip'
!    print*, minval(sib%prog%cupr2    ),maxval(sib%prog%cupr2),' Convective '
!    print*, minval(sib%prog%sw_dwn2),maxval(sib%prog%sw_dwn2),  &
!        ' Short wave down'
!    print*,'-----------------------------------------------------'


end subroutine sibdrv_read_ncep2_single

